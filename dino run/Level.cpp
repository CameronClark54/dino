#include "Level.h"
#include "Game.h"
#include "AssetManager.h"
#include <stdlib.h>


LevelScreen::LevelScreen(Game* newGamePointer)
	:playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, Enemy()
	, groundInstance()
	, flyingInstance()
	, EnemiesGap(0)
	, score(0)
	, gameFont(AssetManager::RequestFont("Assets/Font/enter-command.ttf"))
{
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(500, 20);

	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.setVolume(25);
	gameMusic.play();
}

void LevelScreen::Input()
{
	//playerInstance.Input();
	if (playerInstance.GetAlive()) 
	{
		playerInstance.Input(gamePointer->GetWindow().getSize());
	}
}

void LevelScreen::Update(sf::Time frameTime, sf::Vector2u screenSize)
{

	if (playerInstance.GetAlive()==true) 
	{
		playerInstance.Update(frameTime, screenSize);
		for (int i = 0; i < Enemy.size(); ++i)
		{
			playerInstance.HandleSolidCollision(Enemy[i]->GetHitbox());
		}

		//updates and displays score
		score = score + frameTime.asSeconds()*5;
		scoreText.setString("Score: " + std::to_string((int)score));

		EnemiesGap = EnemiesGap + frameTime.asSeconds();

		if (EnemiesGap > 5)
		{
			AddEnemies();
		}

		//update Enemies
		for (int i = 0; i < Enemy.size(); ++i)
		{
			Enemy[i]->Update(frameTime,screenSize);
		}


	}
	else
	{
		gameMusic.stop();

	}


}

void LevelScreen::DrawTo(sf::RenderTarget & target)
{

	//draw things on screen
	playerInstance.DrawTo(target);

	for (int i = 0; i < Enemy.size(); ++i)
	{
		Enemy[i]->DrawTo(target);
	}

	gamePointer->GetWindow().draw(scoreText);
}

void LevelScreen::AddEnemies()
{


	//creates a new Enemies
	Enemies* newEnemies = nullptr;

	//chooses the type of Enemies to create
	int choiceMin = 0;
	int choiceMax = 100;
	int choice = rand() % (choiceMax - choiceMin) + choiceMin;
	int chanceMoving = 50;
	if (choice < 50)
	{
		newEnemies = new MovingEnemies();
		Enemy.push_back(newEnemies);
	}
	else
	{
		newEnemies = new Enemies();
		Enemy.push_back(newEnemies);
	}
	EnemiesGap = 0;
}