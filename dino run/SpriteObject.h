#pragma once
#include <SFML/Graphics.hpp>
class SpriteObject
{
public:
	// constructors / destructors
	SpriteObject(sf::Texture& newTexture);
	
	//functions
	void DrawTo(sf::RenderTarget& target);

	//Getter
	//sf::FloatRect GetHitbox();

	virtual sf::FloatRect GetHitbox();
	sf::Vector2f CalculateCollisionDepth(sf::FloatRect otherHitbox);


protected:
	// Data
	sf::Sprite sprite;
	//sf::Vector2f CalculateCollisionDepth(sf::FloatRect otherHitbox);
};
