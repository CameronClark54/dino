#pragma once
#include "AnimatingObject.h"
#include <SFML/Audio.hpp>
class Player :
	public AnimatingObject
{
	public:
	//constructor
	Player(sf::Vector2u screenSize);

	//Functions
	void Input(sf::Vector2u screenSize);
	void Update(sf::Time frameTime, sf::Vector2u screenSize);
	bool HandleSolidCollision(sf::FloatRect otherHitbox);
	sf::FloatRect GetHitBox();
	bool GetAlive();

	private:
	
	sf::Vector2f velocity;
	float speed;
	float gravity;
	sf::Vector2f previousPosition;
	bool isCrouching;
	bool isAlive;
	float speedUp;
	bool isJumping;
	sf::SoundBuffer deathSoundBuffer;
	sf::Sound Death;
};

