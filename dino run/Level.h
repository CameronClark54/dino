#pragma once
#include "Player.h"
#include "Enemies.h"
#include "MovingEnemies.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
class Game;
class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);
	void Input();
	void Update(sf::Time frameTime, sf::Vector2u screenSize);
	void DrawTo(sf::RenderTarget& target);
	void AddEnemies();

private:
	Player playerInstance;
	std::vector<Enemies*> Enemy;
	float EnemiesGap;
	Enemies groundInstance;
	MovingEnemies flyingInstance;
	sf::Text scoreText;
	sf::Font gameFont;
	sf::Music gameMusic;
	float score;
	Game* gamePointer;
};
