#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2u screenSize)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/DinoMovement.png"), 100, 80, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(400.0f)
	, gravity(1200.0f)
	, isCrouching(false)
	, isAlive(true)
	,speedUp(0)
	,isJumping(false)
{
	AddClip("Run", 0, 1);
	AddClip("Crouch", 2, 3);
	AddClip("Jump", 4, 5);
	AddClip("Dead", 6, 6);
	PlayClip("Run", true);
	

	// position the player at the centre of the screen
	sf::Vector2f newPosition;
	newPosition.x = ((float)screenSize.x - sprite.getGlobalBounds().width) / 3.0f;
	newPosition.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);

	deathSoundBuffer.loadFromFile("Assets/Audio/death.wav");
	Death.setBuffer(deathSoundBuffer);
	Death.setVolume(25);
}

void Player::Input(sf::Vector2u screenSize)
{
	
	velocity.x = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && isJumping==false)
	{
		velocity.y = speed;
		//set the upward velocity to a jump value.
		const float JUMP_VALUE = -600; // negative to go up
		velocity.y = JUMP_VALUE;
		isJumping = true;

		if (sprite.getPosition().y > (2 * screenSize.y) / 3)
		{
			PlayClip("Jump", true);
		}
		else
		{
			PlayClip("Run", true);
			isJumping = false;
		}
		
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		PlayClip("Crouch", true);
		isCrouching = true;
	}
	else
	{
		PlayClip("Run", true);
		isCrouching = false;
	}

}

void Player::Update(sf::Time frameTime, sf::Vector2u screenSize)
{
	//sets previous position
	previousPosition = sprite.getPosition();

	//calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	if (newPosition.y > 2 * screenSize.y / 3) 
	{
		newPosition.y = previousPosition.y;
		velocity.y = 0.0f;
	}
	//move player to the new position
	sprite.setPosition(newPosition);
	AnimatingObject::Update(frameTime);

	//calculate new velocity
	velocity.y = velocity.y + gravity * frameTime.asSeconds();


	speedUp = speedUp+ frameTime.asSeconds();
	if (speedUp == 30) 
	{
		speed = speed + 50.0f;
		speedUp = 0.0f;
	}
}

bool Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	//if player collides with enemy
	if (GetHitbox().intersects(otherHitbox)) 
	{
		isAlive = false;
		Death.play();
	}
	return isAlive;
}

sf::FloatRect Player::GetHitBox()
{
	//get original hitbox
	sf::FloatRect normalHitbox = SpriteObject::GetHitbox();

	sf::FloatRect crouchedHitbox = normalHitbox;

	if (isCrouching == true)
	{
		//resize new hitbox so it fits over crouch sprite 
		crouchedHitbox.height = crouchedHitbox.height / 2;
		crouchedHitbox.top += normalHitbox.height * (1 / 2);
	}
	
	return crouchedHitbox;
}

bool Player::GetAlive()
{
	return isAlive;
}
