#include "Enemies.h"
#include "AssetManager.h"

Enemies::Enemies()
:SpriteObject(AssetManager::RequestTexture("Assets/Graphics/cactus.png"))
, velocity(150)
{
	sprite.setPosition(2000,700);
}

Enemies::Enemies(sf::Texture & newTexture, sf::Vector2f newPosition)
: SpriteObject(newTexture)
,velocity(150)
{
	sprite.setPosition(newPosition);
}


void Enemies::Update(sf::Time frameTime, sf::Vector2u screenSize)
{
	sf::Vector2f newPos = sprite.getPosition();
	newPos.x -= velocity * frameTime.asSeconds();
	sprite.setPosition(newPos);
}
