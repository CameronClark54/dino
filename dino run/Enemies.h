#pragma once
#include "SpriteObject.h"
#include <stdlib.h>
class Enemies :
	public SpriteObject
{
public:
	// Constructors
	Enemies();
	Enemies(sf::Texture& newTexture, sf::Vector2f newPosition);
	virtual void Update(sf::Time frameTime, sf::Vector2u screenSize);

private:
	int velocity;
	
};

